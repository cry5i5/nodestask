package com.example.abhishektyagi1.yoyocinema.ui.movieDetails

import android.arch.lifecycle.ViewModel
import com.example.abhishektyagi1.yoyocinema.data.models.MovieDetail
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import io.reactivex.Single


class MovieDetailViewModel(val movieDataRepository: MovieDataRepository) : ViewModel() {

    fun getMovieDetails(id: Long): Single<MovieDetail> {
        return movieDataRepository.getMovieDetails(id)
    }
}