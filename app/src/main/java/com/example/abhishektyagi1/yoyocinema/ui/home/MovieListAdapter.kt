package com.example.abhishektyagi1.yoyocinema.ui.home


import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.example.abhishektyagi1.yoyocinema.R
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import com.example.abhishektyagi1.yoyocinema.utils.PosterSize
import com.example.abhishektyagi1.yoyocinema.utils.Utility
import com.squareup.picasso.Picasso
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.fragment_search.view.*

class MovieListAdapter
    : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {
    var mValues = ArrayList<Movie>()

    val clickObservable: PublishSubject<Long> = PublishSubject.create()
    val favoriteClickObservable: PublishSubject<Int> = PublishSubject.create()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_search, parent, false)
        return ViewHolder(view)
    }

    fun updateList(list: List<Movie>) {
        mValues.clear()
        mValues.addAll(list)
        notifyDataSetChanged()
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = mValues[position]
        holder.title.text = movie.title
        if (movie.isFavorite) {
            holder.favorite.setImageResource(R.drawable.ic_favorite_black_24dp)
        } else {
            holder.favorite.setImageResource(R.drawable.ic_favorite_border_black_24dp)
        }

        holder.overview.text = movie.overview

        if (movie.posterPath != null) {
            Picasso.get().load(Utility.getMoviePosterPath(movie.posterPath, PosterSize.W92))
                    .into(holder.poster)
        }

    }

    override fun getItemCount(): Int = mValues.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView), View.OnClickListener {
        val poster: ImageView = mView.poster
        val title: TextView = mView.title
        val overview: TextView = mView.overview
        val favorite: ImageView = mView.favorite

        init {
            mView.setOnClickListener(this)
            favorite.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            when (v?.id) {
                favorite.id -> {

                    favoriteClickObservable.onNext(adapterPosition)
                }
                mView.id -> clickObservable.onNext(mValues[adapterPosition].id)
            }
        }
    }
}
