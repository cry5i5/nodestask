package com.example.abhishektyagi1.yoyocinema.data.sources.remote

import com.example.abhishektyagi1.yoyocinema.data.models.ApiResponse
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import com.example.abhishektyagi1.yoyocinema.data.models.MovieDetail
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


class RemoteDataSource {
    val mRetrofit: Retrofit
    init {
        mRetrofit = Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }

    val API_KEY = "4cb1eeab94f45affe2536f2c684a5c9e"
    interface MovieApiService {
        @GET("search/movie")
        fun getMovies(@Query("api_key") apiKey: String
                      , @Query("query") query: String): Single<ApiResponse>

        @GET("movie/{id}")
        fun getMovieDetails(
                @Path("id") movieId: Long,
                @Query("api_key") apiKey: String): Single<MovieDetail>

    }

    fun getSearchResults(query: String): Single<List<Movie>> {
        val movieApi = mRetrofit.create(MovieApiService::class.java)
        return movieApi.getMovies(API_KEY, query)
                .map {response ->
                    response.results
                }
    }

    fun getMovieDetails(movieId: Long): Single<MovieDetail> {
        val movieApi = mRetrofit.create(MovieApiService::class.java)
        return movieApi.getMovieDetails(movieId, API_KEY)
    }

}