package com.example.abhishektyagi1.yoyocinema.utils


object Utility {

    fun getMoviePosterPath(path: String, size: PosterSize): String {
        return "https://image.tmdb.org/t/p/${size.sizeString}$path"
    }
}