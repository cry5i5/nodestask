package com.example.abhishektyagi1.yoyocinema.ui.home

import android.arch.lifecycle.ViewModel
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.PublishSubject


class HomeActivityViewModel(val movieDataRepository: MovieDataRepository) :ViewModel() {

    var searchSubject: PublishSubject<List<Movie>> = PublishSubject.create()
    var favoriteMovies = HashSet<Long>()

    fun search(query: String): Single<List<Movie>> {
        return movieDataRepository.searchMovie(query)
    }


    // This function is used to make sure searched movies also have favorite marked on them if they are in favorite list
    fun loadFavoriteMovies() {
        movieDataRepository.loadFavoriteMoviesIds()
                .subscribeOn(Schedulers.io())
                .subscribe {
//                    favoriteMovies.add(it)
                    favoriteMovies.addAll(it)
                }
    }

    fun toggleFavorite(movie: Movie): Completable {
        if (movie.isFavorite) {
            return movieDataRepository.insertFavoriteMovie(movie)
        }
        return movieDataRepository.removeFavoriteMovie(movie)

    }

    fun getFavouriteMovies(): Single<List<Movie>> {
        return movieDataRepository.getFavoriteMovies()
    }
}