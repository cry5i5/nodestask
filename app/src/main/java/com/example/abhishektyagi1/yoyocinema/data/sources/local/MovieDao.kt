package com.example.abhishektyagi1.yoyocinema.data.sources.local

import android.arch.persistence.room.*
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import io.reactivex.Flowable
import io.reactivex.Single

@Dao
interface MovieDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMovie(movie: Movie)

    @Delete
    fun removeMovie(movie: Movie)

    @Query("Select id from ${AppDatabase.MOVIE_TABLE}")
    fun getAllFavoriteMovies(): Flowable<List<Long>>

    @Query("Select * from ${AppDatabase.MOVIE_TABLE}")
    fun getFavouriteMovies(): Single<List<Movie>>
}