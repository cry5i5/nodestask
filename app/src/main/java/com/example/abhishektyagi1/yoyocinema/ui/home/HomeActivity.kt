package com.example.abhishektyagi1.yoyocinema.ui.home

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import com.example.abhishektyagi1.yoyocinema.R
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import com.example.abhishektyagi1.yoyocinema.utils.ViewModelFactory


/*
* This is the main class. I've created 2 fragments for search and favorite list.
* You can tap search list or favorite list to open Movie Detail Screen.
* */


class HomeActivity : AppCompatActivity() {

    private var mSearchFragment: SearchListFragment? = null
    private var mFavoriteListFragment: FavoriteListFragment? = null

    private val mViewModel: HomeActivityViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders
                .of(this, ViewModelFactory(MovieDataRepository.getInstance(this)))
                .get(HomeActivityViewModel::class.java)
    }

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_search -> {
                if (mSearchFragment == null) {
                    mSearchFragment = SearchListFragment()
                }
                loadFragment(mSearchFragment!!)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_favorite -> {
                if (mFavoriteListFragment == null) {
                    mFavoriteListFragment = FavoriteListFragment()
                }
                loadFragment(mFavoriteListFragment!!)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun loadFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_movie)
        mSearchFragment = SearchListFragment()
        loadFragment(mSearchFragment!!)
        val navigation = findViewById<BottomNavigationView>(R.id.navigation)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        mViewModel.loadFavoriteMovies()
    }
}
