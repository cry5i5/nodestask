package com.example.abhishektyagi1.yoyocinema.ui.home

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.abhishektyagi1.yoyocinema.R
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import com.example.abhishektyagi1.yoyocinema.ui.movieDetails.MovieDetailActivity
import com.example.abhishektyagi1.yoyocinema.utils.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SearchListFragment : Fragment() {

    var columnCount = 1
    private val mViewModel: HomeActivityViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders
                .of(activity!!, ViewModelFactory(MovieDataRepository.getInstance(context!!)))
                .get(HomeActivityViewModel::class.java)
    }

    lateinit var mAdapter: MovieListAdapter
    var disposable = CompositeDisposable()

    lateinit var emptyScreenMsg: TextView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_search_list, container, false)
        val recyclerView = view.findViewById<RecyclerView>(R.id.list)
        recyclerView.layoutManager = LinearLayoutManager(context)
        mAdapter = MovieListAdapter()
        recyclerView.adapter = mAdapter
        val searchView = view.findViewById<SearchView>(R.id.searchField)
        emptyScreenMsg = view.findViewById(R.id.emptyScreenMsg)
        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    search(query)
                }
                return true
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return true
            }
        })

        disposable.add(mAdapter.clickObservable
                .subscribe {
                    val intent = Intent(context, MovieDetailActivity::class.java)
                    intent.putExtra(MovieDetailActivity.MOVIE_DETAIL_ID, it)
                    startActivity(intent)
                })

        disposable.add(mAdapter.favoriteClickObservable
                .subscribe {
                    val movie = mAdapter.mValues[it]
                    movie.isFavorite = !movie.isFavorite
                    mAdapter.notifyItemChanged(it)
                    mViewModel.toggleFavorite(movie)
                            .subscribeOn(Schedulers.io())
                            .subscribe()
                })

        return view
    }

    private fun search(query: String) {
        disposable.add(mViewModel.search(query)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({list ->
                    Log.d("Search", "List received with items ${list.size}")
                    list.forEach {
                        it.isFavorite = mViewModel.favoriteMovies.contains(it.id)
                    }
                    emptyScreenMsg.visibility = View.INVISIBLE
                    mAdapter.updateList(list)
                }, {error ->
                    Log.d("Search", error.localizedMessage)
                }))
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }
}
