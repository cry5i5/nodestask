package com.example.abhishektyagi1.yoyocinema.data.models

import com.google.gson.annotations.SerializedName


data class MovieDetail(val budget: Long,
                       val overview: String,
                       @SerializedName("poster_path")
                       val posterPath: String?,
                       @SerializedName("release_date")
                       val releaseData: String?,
                       val tagline: String,
                       val title: String,
                       @SerializedName("vote_average")
                       val rating: Float)