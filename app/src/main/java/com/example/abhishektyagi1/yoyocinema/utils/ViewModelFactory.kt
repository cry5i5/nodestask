package com.example.abhishektyagi1.yoyocinema.utils

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import com.example.abhishektyagi1.yoyocinema.ui.home.HomeActivityViewModel
import com.example.abhishektyagi1.yoyocinema.ui.movieDetails.MovieDetailViewModel


class ViewModelFactory( val repository: MovieDataRepository) : ViewModelProvider.Factory {


    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass == HomeActivityViewModel::class.java) {
            return HomeActivityViewModel(repository) as T
        }
        if (modelClass == MovieDetailViewModel::class.java) {
            return MovieDetailViewModel(repository) as T
        }
        throw IllegalArgumentException("unknown model class " + modelClass)
    }
}