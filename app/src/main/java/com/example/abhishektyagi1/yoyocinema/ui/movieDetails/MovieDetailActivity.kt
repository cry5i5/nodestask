package com.example.abhishektyagi1.yoyocinema.ui.movieDetails

import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.ImageView
import android.widget.TextView
import com.example.abhishektyagi1.yoyocinema.R
import com.example.abhishektyagi1.yoyocinema.data.models.MovieDetail
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import com.example.abhishektyagi1.yoyocinema.utils.PosterSize
import com.example.abhishektyagi1.yoyocinema.utils.Utility
import com.example.abhishektyagi1.yoyocinema.utils.ViewModelFactory
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MovieDetailActivity : AppCompatActivity() {

    companion object {
        val MOVIE_DETAIL_ID = "MOVIE_DETAIL_ID"
    }

    private val mViewModel: MovieDetailViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders
                .of(this, ViewModelFactory(MovieDataRepository.getInstance(this)))
                .get(MovieDetailViewModel::class.java)
    }

    lateinit var mTitle: TextView
    lateinit var mTagline: TextView
    lateinit var mOverview: TextView
    lateinit var mRating: TextView
    lateinit var mMoviePoster: ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)

        val movieId = intent.getLongExtra(MOVIE_DETAIL_ID, -1)
        if (movieId == -1L) {
            finish()
        } else {
            mViewModel.getMovieDetails(movieId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { movieDetail, error ->
                        if (error != null) {
                            // Show Error
                        } else {
                            setupUI(movieDetail)
                        }
                    }
        }
        mTitle = findViewById(R.id.movieTitle)
        mTagline = findViewById(R.id.tagline)
        mOverview = findViewById(R.id.overview)
        mMoviePoster = findViewById(R.id.moviePoster)
        mRating = findViewById(R.id.ratingVal)
    }

    private fun setupUI(movieDetail: MovieDetail) {
        mTitle.text = movieDetail.title
        mTagline.text = movieDetail.tagline
        mOverview.text = movieDetail.overview
        mRating.text = "${movieDetail.rating}"
        if (movieDetail.posterPath != null) {
            Picasso.get().load(Utility.getMoviePosterPath(movieDetail.posterPath, PosterSize.W185))
                    .into(mMoviePoster)
        }
    }
}
