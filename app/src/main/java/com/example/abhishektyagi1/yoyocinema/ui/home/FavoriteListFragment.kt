package com.example.abhishektyagi1.yoyocinema.ui.home


import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.example.abhishektyagi1.yoyocinema.R
import com.example.abhishektyagi1.yoyocinema.data.sources.MovieDataRepository
import com.example.abhishektyagi1.yoyocinema.ui.movieDetails.MovieDetailActivity
import com.example.abhishektyagi1.yoyocinema.utils.ViewModelFactory
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class FavoriteListFragment : Fragment() {

    lateinit var mAdapter: MovieListAdapter

    private val mViewModel: HomeActivityViewModel by lazy(LazyThreadSafetyMode.NONE) {
        ViewModelProviders
                .of(activity!!, ViewModelFactory(MovieDataRepository.getInstance(context!!)))
                .get(HomeActivityViewModel::class.java)
    }

    var disposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_favorite_list, container, false)

        mAdapter = MovieListAdapter()
        if (view is RecyclerView) {
           with(view) {
               layoutManager = LinearLayoutManager(context)
               adapter = mAdapter
           }
        }

        disposable.add(mViewModel.getFavouriteMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { list ->
                    mAdapter.updateList(list)
                })

        disposable.add(mAdapter.clickObservable
                .subscribe {
                    val intent = Intent(context, MovieDetailActivity::class.java)
                    intent.putExtra(MovieDetailActivity.MOVIE_DETAIL_ID, it)
                    startActivity(intent)
                })

        disposable.add(mAdapter.favoriteClickObservable
                .subscribe {
                    val movie = mAdapter.mValues[it]
                    movie.isFavorite = !movie.isFavorite
                    mViewModel.toggleFavorite(movie)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe {
                                mAdapter.mValues.removeAt(it)
                                mAdapter.notifyItemRemoved(it)
                            }
                })

        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        disposable.clear()
    }
}
