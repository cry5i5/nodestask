package com.example.abhishektyagi1.yoyocinema.data.models

import com.google.gson.annotations.SerializedName
import retrofit2.http.Field


data class ApiResponse(
        val page: Int
        , val results: List<Movie>)