package com.example.abhishektyagi1.yoyocinema.data.sources.local

import android.content.Context
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import io.reactivex.Completable


class LocalDataSource(val context: Context) {

    fun insertFavorite(movie: Movie): Completable {
        return Completable.fromAction {
            AppDatabase.getInstance(context).movieDao().insertMovie(movie)
        }

    }

    fun removeFavorite(movie: Movie): Completable {
        return Completable.fromAction {
            AppDatabase.getInstance(context).movieDao().removeMovie(movie)
        }
    }

    fun loadFavoriteMoviesIds() = AppDatabase.getInstance(context).movieDao().getAllFavoriteMovies()

    fun getFavoriteMovies() = AppDatabase.getInstance(context).movieDao().getFavouriteMovies()
}