package com.example.abhishektyagi1.yoyocinema.utils


enum class PosterSize(val sizeString: String) {
    W92("w92"),
    W154("w154"),
    W185("w185"),
    W342("w342"),
    W500("w500"),
    W780("w780"),
    ORIGINAL("original")

}