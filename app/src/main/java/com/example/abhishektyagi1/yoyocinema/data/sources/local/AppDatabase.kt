package com.example.abhishektyagi1.yoyocinema.data.sources.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.example.abhishektyagi1.yoyocinema.data.models.Movie


@Database(entities = arrayOf(Movie::class),
        version = 1)
abstract class AppDatabase: RoomDatabase() {


    companion object {
        private var instance: AppDatabase? = null
        @Synchronized
        fun getInstance(context: Context): AppDatabase {
            if (instance == null) {
                instance = Room
                        .databaseBuilder<AppDatabase>(context.applicationContext, AppDatabase::class
                                .java, "yoyoCinema.db")
                        .build()
            }
            return instance!!
        }

        fun destroyInstance() {
            instance = null
        }

        const val MOVIE_TABLE = "movies"
    }

    abstract fun movieDao(): MovieDao

}