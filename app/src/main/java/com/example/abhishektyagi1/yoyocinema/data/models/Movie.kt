package com.example.abhishektyagi1.yoyocinema.data.models

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.example.abhishektyagi1.yoyocinema.data.sources.local.AppDatabase
import com.google.gson.annotations.SerializedName

@Entity(tableName = AppDatabase.MOVIE_TABLE)
data class Movie(
        @PrimaryKey
        val id: Long,
        val title: String,
        @SerializedName("poster_path")
        val posterPath: String?,
        val overview: String?,
        var isFavorite: Boolean = false)