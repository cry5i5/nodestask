package com.example.abhishektyagi1.yoyocinema.data.sources

import android.content.Context
import com.example.abhishektyagi1.yoyocinema.data.models.Movie
import com.example.abhishektyagi1.yoyocinema.data.sources.local.AppDatabase
import com.example.abhishektyagi1.yoyocinema.data.sources.local.LocalDataSource
import com.example.abhishektyagi1.yoyocinema.data.sources.remote.RemoteDataSource
import io.reactivex.Observable
import io.reactivex.Single


//This class decides whether data needs to be fetched from local or remote
class MovieDataRepository private constructor(context: Context) {

    companion object {
        private var instance: MovieDataRepository? = null
        @Synchronized
        fun getInstance(context: Context): MovieDataRepository {
            if (instance == null) {
                instance = MovieDataRepository(context)
            }
            return instance!!
        }

    }
    private val localDataSource: LocalDataSource
    private val remoteDataSource: RemoteDataSource

    init {
        remoteDataSource = RemoteDataSource()
        localDataSource = LocalDataSource(context)
    }

    fun searchMovie(query: String): Single<List<Movie>> {
        return remoteDataSource.getSearchResults(query)
    }

    fun getMovieDetails(movieId: Long) = remoteDataSource.getMovieDetails(movieId)

    fun loadFavoriteMoviesIds() = localDataSource.loadFavoriteMoviesIds()


    fun insertFavoriteMovie(movie: Movie) = localDataSource.insertFavorite(movie)

    fun removeFavoriteMovie(movie: Movie) = localDataSource.removeFavorite(movie)

    fun getFavoriteMovies() = localDataSource.getFavoriteMovies()

}